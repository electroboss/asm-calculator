%include "library.asm"

section .text
global _start
_start:
  mov eax,4
  mov ebx,1
  mov ecx,firstnum_prompt
  mov edx,firstnum_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,firstnumstr
  mov edx,6 ; five digits max (up to 65536), plus newline
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,secondnum_prompt
  mov edx,secondnum_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,secondnumstr
  mov edx,6 ; five digits max (up to 65536), plus newline
  int 80h

  ; find length
  mov eax,firstnumstr
  mov ebx,5
  call findnumstrlen

  ; parse
  ;mov bx,bx
  mov edx,firstnumstr
  call numparse ; parse number
  mov [firstnum],cx

  ; same again for secondnumstr
  mov eax,secondnumstr
  mov ebx,5
  call findnumstrlen
  ;mov bx,bx
  mov edx,secondnumstr
  call numparse
  mov [secondnum],cx

  mov eax,4
  mov ebx,1
  mov ecx,operator_prompt
  mov edx,operator_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,operator
  mov edx,1
  int 80h

  mov ebx,0
  mov ecx,0
  mov al,[operator]
  mov bx,[firstnum]
  mov cx,[secondnum]
  cmp al,"+"
  je .addition
  cmp al,"-"
  je .subtraction
  cmp al,"*"
  je .multiplication
  cmp al,"/"
  je .division
  jmp .invalid

.addition:
  add ebx,ecx
  jmp .print
.subtraction:
  sub ebx,ecx
  jmp .print
.multiplication:
  mov ax,bx
  mul cx
  shl edx,16
  mov dx,ax
  mov ebx,edx
  jmp .print
.division:
  mov ax,bx
  mov dx,0
  div cx

  push dx
  push ax

  mov eax,4
  mov ebx,1
  mov ecx,quotient
  mov edx,quotient_len
  int 80h

  pop ax
  mov ecx,outputstring+9
  call numtostr

  mov eax,4
  mov ebx,1
; mov ecx,ecx
; mov edx,edx
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,remainder
  mov edx,remainder_len
  int 80h

  pop ax
  mov ecx,outputstring+9
  call numtostr

  mov eax,4
  mov ebx,1
; mov ecx,ecx
; mov edx,edx
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,newline
  mov edx,1
  int 80h

  jmp .end
.invalid:
  mov eax,4
  mov ebx,1
  mov ecx,invalid_char
  mov edx,invalid_char_len
  int 80h
  jmp .end

.print:
  mov eax,ebx
  mov ecx,outputstring+9
  call numtostr

  mov eax,4
  mov ebx,1
; mov edx,edx
; mov ecx,ecx
  int 80h
  mov eax,4
  mov ebx,1
  mov ecx,newline
  mov edx,1
  int 80h

  ; jmp .end

.end:
  mov eax,1
  mov ebx,0
  int 80h


section .data
  firstnum_prompt db "Enter first number",0Ah
  firstnum_prompt_len equ $ - firstnum_prompt
  secondnum_prompt db "Enter second number",0Ah
  secondnum_prompt_len equ $ - secondnum_prompt
  operator_prompt db "Enter operator",0Ah
  operator_prompt_len equ $ - operator_prompt
  invalid_char db "Invalid char. Quitting.",0Ah
  invalid_char_len equ $ - invalid_char
  newline db 0Ah
  quotient db "Quotient: "
  quotient_len equ $ - quotient
  remainder db 0Ah,"Remainder: "
  remainder_len equ $ - remainder
section .bss
  firstnumstr resb 5
  secondnumstr resb 5
  firstnum resw 1
  secondnum resw 1
  operator resb 1
  outputstring resb 10 ; for max value of 65535*65535=4294836225