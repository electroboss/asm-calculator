section .text
numparse:
  ; finds numerical value for base-10
  ; string at edx, with length bx
  ; and store in cx
  
  mov ecx,0
  ; for every digit, starting with most significant, times by ten ah times and add to ecx
.loop:
  push edx
  mov dl,[edx]
  sub dl,'0'
  mov dh,0

  push bx
.powerloop:
  dec bx
  cmp bx,0
  je .powerend

  mov ax,10
  mul dx ; multiply dx (the digit) by 10
  mov dx,ax
  jmp .powerloop
.powerend:

  add cx,dx
  pop bx
  pop edx

  inc edx
  dec bx
  cmp bx,0
  jne .loop

;.done:
  ret


findnumstrlen:
  ; finds length of string at eax,
  ; with ebx being the string length 
  add ebx,eax
.findlenstart:
  dec ebx
  cmp byte [ebx],'0'
  jge .findlenend
  cmp ebx,eax
  jne .findlenstart
.findlenend:
  sub ebx,eax
  inc ebx
  ret

numtostr:
  push ecx
  mov ebx,10
.loop:
  mov edx,0
  div ebx
  add edx,'0'
  mov [ecx],dl ; facepalm - originally was edx. sigh...
  dec ecx
  cmp eax,0
  jne .loop

  pop edx
  sub edx,ecx
  inc ecx
  ret
;   RUBBISH CODE FOR DESTROYIFICATION (but just in case the new code doesn't work...)
; mov ecx,10000
; mov eax,ebx
; mov edx,0
; div ecx
; add eax,'0'
; mov [outputstring],al
; mov eax,edx
; mov edx,0
; mov ecx,1000
; div ecx
; add eax,'0'
; mov [outputstring+1],al
; mov eax,edx
; mov edx,0
; mov ecx,100
; div ecx
; add eax,'0'
; mov [outputstring+2],al
; mov eax,edx
; mov edx,0
; mov ecx,10
; div ecx
; add al,'0'
; add dl,'0'
; mov [outputstring+3],al
; mov [outputstring+4],dl
; ret